FROM debian:testing
ENV DEBIAN_FRONTEND noninteractive
WORKDIR /cory
# ------------------------------- Dependencies --------------------------------
# Base tooling
# Base tooling
RUN apt-get update && apt-get install -qq build-essential vim apt-utils curl \
    jq libdrm-dev tree ncdu ripgrep mc wget pkg-config cmake python3-pip \
    # Debian packaging tools
    devscripts git-buildpackage sbuild apt-rdepends piuparts autopkgtest lintian \
    # yes, I really tried sbuild inside docker...
    sbuild-debian-developer-setup \
    # ROCm build dependencies 
    #RUN apt update && apt install -y llvm-13 llvm-13-dev clang-13 libclang-13-dev lld-13 liblld-13-dev
    llvm-dev clang libclang-dev clang-tools lld liblld-13-dev \
    kmod msr-tools libelf-dev zlib1g-dev libnuma-dev libudev-dev mesa-common-dev \
    python3.9-venv python3-virtualenv python3-yaml python3-msgpack gfortran \
    libmsgpack-dev libfmt-dev
# ------------------------------- Environment ---------------------------------
#RUN apt install -y sudo
#RUN adduser --disabled-password --gecos '' myuser
#RUN adduser myuser sudo
#RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> \
#    /etc/sudoers

# Make myself at home
COPY misc/dotfiles/* /root/

# Pervase UID/GID
COPY ./cory/*id.txt /root/
# This was hackily written by ./2_*.sh
#RUN usermod -u $(cat /home/myuser/uid.txt) myuser

#RUN adduser myuser sbuild
RUN adduser root video
RUN groupadd render
# This was hackily written by ./2_*.sh too
RUN groupmod -g $(cat /root/render_gid.txt) render
RUN adduser root render

# -------------------------------- File setup ---------------------------------
#COPY ./misc/dotfiles/* /home/myuser/
# -------------------------------- Misc ---------------------------------
# yes, I really tried sbuild inside docker...
#RUN apt install -y sbuild-debian-developer-setup
RUN apt install -y kmod msr-tools
#USER myuser
