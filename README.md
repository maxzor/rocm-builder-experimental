# Aim
This repo was made to verify if the current ROCm release could be \
compiled with upstream LLVM instead of the AMD LLVM fork. \
The answer is yes, but it requires tweaks, \
and not all functionality seems present. \
See the result directory and https://github.com/ROCm-Developer-Tools/HIP/issues/2449

# Build
setup_docker.sh to start, \
`{amd,vanilla}_llvm-rocm-install.sh` inside docker.

# Disclaimer
There is a red-light warranty with this repo: \
the moment you clone it I hop in my car, \
as soon as you don't see my red lights anymore, \
support for the repo is gone :)
