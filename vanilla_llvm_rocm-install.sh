#!/usr/bin/env bash

set -exuo pipefail

# Workaround to llvm-defaults(:13) being a bit broken
ln -s /usr/bin/clang-offload-bundler-13 /usr/bin/clang-offload-bundler

export CC=clang
export CXX=clang++

DEB_WORKSPACE=$HOME # where to download and build the sources
DEB_HIP_ARCHITECTURES='gfx906:xnack-' # https://llvm.org/docs/AMDGPUUsage.html

# rocm-cmake
cd "$DEB_WORKSPACE"
wget -qO- https://github.com/RadeonOpenCompute/rocm-cmake/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd rocm-cmake-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# rocm-device-libs
cd "$DEB_WORKSPACE"
wget -qO- https://github.com/RadeonOpenCompute/ROCm-Device-Libs/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd ROCm-Device-Libs-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_C_COMPILER=/usr/bin/clang \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# roct-thunk-interface
cd "$DEB_WORKSPACE"
apt-get -qq install libnuma-dev pkg-config libdrm-dev
wget -qO- https://github.com/RadeonOpenCompute/ROCT-Thunk-Interface/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd ROCT-Thunk-Interface-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# rocr-runtime
cd "$DEB_WORKSPACE"
apt-get -qq install libelf-dev xxd git-buildpackage
wget -qO- https://github.com/RadeonOpenCompute/ROCR-Runtime/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd ROCR-Runtime-rocm-4.5.2
cmake -Ssrc -Bbuild \
  -DCMAKE_INSTALL_PREFIX=/usr -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld
make -j32 -C build
make -C build install

# rocminfo
cd "$DEB_WORKSPACE"
apt-get -qq install kmod python3
wget -qO- https://github.com/RadeonOpenCompute/rocminfo/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd rocminfo-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# comgr
cd "$DEB_WORKSPACE"
wget https://github.com/RadeonOpenCompute/ROCm-CompilerSupport/archive/refs/heads/amd-stg-open.zip
unzip amd-stg-open.zip
tar czf rocm-comgr_4.5.2.1.tar.gz ROCm-CompilerSupport-amd-stg-open
git clone https://salsa.debian.org/maxzor/rocm-compilersupport rocm-compilersupport
cd rocm-compilersupport
git checkout upstream && git checkout pristine-tar && git checkout master
echo | gbp import-orig ../rocm-comgr_4.5.2.1.tar.gz
#Removing package dependency as the software needed to build has been installed out of the dpkg system.
sed --in-place 's/rocm-device-libs,//g;' debian/control
git commit -am "yo"
gbp buildpackage --git-ignore-new
dpkg -i ../*.deb

# hip
cd "$DEB_WORKSPACE"
apt-get -qq install mesa-common-dev
wget -qO- https://github.com/ROCm-Developer-Tools/hipamd/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
wget -qO- https://github.com/ROCm-Developer-Tools/ROCclr/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
wget -qO- https://github.com/RadeonOpenCompute/ROCm-OpenCL-Runtime/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
# A fix for clang version: https://github.com/ROCm-Developer-Tools/HIP/pull/2451 ----------------------------------
git clone https://github.com/Maxzor/HIP HIP-rocm-4.5.2
cd hipamd-rocm-4.5.2
mkdir build
cd build
cmake -S.. -B. -DCMAKE_BUILD_TYPE=Release \
  -DHIP_COMMON_DIR="$DEB_WORKSPACE/HIP-rocm-4.5.2" \
  -DAMD_OPENCL_PATH="$DEB_WORKSPACE/ROCm-OpenCL-Runtime-rocm-4.5.2" \
  -DROCCLR_PATH="$DEB_WORKSPACE/ROCclr-rocm-4.5.2" \
  -DCMAKE_HIP_ARCHITECTURES=$DEB_HIP_ARCHITECTURES \
  -DCMAKE_INSTALL_PREFIX=/usr/hip \
  -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld # can't be /usr due to
make -j32
#touch /usr/include/.nodelete
#find /usr/include -type f -exec chattr +i {} \+
make install # warning: will delete $CMAKE_INSTALL_PREFIX/include !
#chattr -i /usr/include/.nodelete
#find /usr/include -type f -exec chattr -i {} \+
#rm /usr/include/.nodelete
apt-get -qq install perl file # used for hipcc

# variables used for projects built with hipcc
export HIP_PLATFORM=amd
export HIP_RUNTIME=rocclr
export HIP_COMPILER=clang
export HIP_CLANG_PATH=/usr/bin
export DEVICE_LIB_PATH=/usr/amdgcn/bitcode
export HIP_DEVICE_LIB_PATH=/usr/amdgcn/bitcode
export HSA_PATH=/usr/hsa
export LLVM_PATH=/usr
export HIP_PATH=/usr/hip
export ROCM_PATH=/usr/hip
export HCC_AMDGPU_TARGET=$DEB_HIP_ARCHITECTURES # workaround for hipcc path issues

# optional hipcc variables -------- disabled parallel jobs... ----------------------------------
# export HIPCC_COMPILE_FLAGS_APPEND='-O3 -Wno-format-nonliteral -parallel-jobs=4'
# export HIPCC_LINK_FLAGS_APPEND='-O3 -parallel-jobs=4'
export HIPCC_COMPILE_FLAGS_APPEND='-O3 -Wno-format-nonliteral'
export HIPCC_LINK_FLAGS_APPEND='-O3'
export HIP_CLANG_HCC_COMPAT_MODE=1

export PATH=$PATH:/usr/hip/bin # find hipcc

# rocrand (with tests)
cd "$DEB_WORKSPACE"
apt-get -qq install gfortran git libgtest-dev
wget -qO- https://github.com/ROCmSoftwarePlatform/rocRAND/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd rocRAND-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DBUILD_TEST=ON \
  -DBUILD_BENCHMARK=ON \
  -DAMDGPU_TARGETS=$DEB_HIP_ARCHITECTURES \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
# make -C build test # uncomment to run the rocrand tests
make -C build install

apt-get -qq install python3-pip
pip3 install remote-pdb

# rocblas
cd "$DEB_WORKSPACE"
apt-get -qq install gfortran libmsgpack-dev python3 python3-pip python3-virtualenv python3.9-venv python3-yaml python3-msgpack
wget -qO- https://github.com/ROCmSoftwarePlatform/rocBLAS/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
wget -qO- https://github.com/ROCmSoftwarePlatform/Tensile/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd rocBLAS-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DTensile_TEST_LOCAL_PATH="$DEB_WORKSPACE/Tensile-rocm-4.5.2" \
  -DTensile_LOGIC=asm_full \
  -DTensile_CODE_OBJECT_VERSION=V3 \
  -DTensile_LIBRARY_FORMAT=msgpack \
  -DAMDGPU_TARGETS=$DEB_HIP_ARCHITECTURES \
  -DRUN_HEADER_TESTING=OFF \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# rocsolver
cd "$DEB_WORKSPACE"
apt-get -qq install gfortran libfmt-dev
wget -qO- https://github.com/ROCmSoftwarePlatform/rocSOLVER/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd rocSOLVER-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DAMDGPU_TARGETS=$DEB_HIP_ARCHITECTURES \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# hipblas (AMD)
cd "$DEB_WORKSPACE"
apt-get -qq install gfortran
wget -qO- https://github.com/ROCmSoftwarePlatform/hipBLAS/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd hipBLAS-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# hipsolver (AMD)
cd "$DEB_WORKSPACE"
apt-get -qq install gfortran
wget -qO- https://github.com/ROCmSoftwarePlatform/hipSOLVER/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd hipSOLVER-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# rocprim
cd "$DEB_WORKSPACE"
apt-get -qq install git
wget -qO- https://github.com/ROCmSoftwarePlatform/rocPRIM/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd rocPRIM-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DAMDGPU_TARGETS=$DEB_HIP_ARCHITECTURES \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# rocsparse
cd "$DEB_WORKSPACE"
apt-get -qq install gfortran
wget -qO- https://github.com/ROCmSoftwarePlatform/rocSPARSE/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd rocSPARSE-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DAMDGPU_TARGETS=$DEB_HIP_ARCHITECTURES \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# hipsparse (AMD)
cd "$DEB_WORKSPACE"
apt-get -qq install gfortran
wget -qO- https://github.com/ROCmSoftwarePlatform/hipSPARSE/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd hipSPARSE-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# rocalution
cd "$DEB_WORKSPACE"
wget -qO- https://github.com/ROCmSoftwarePlatform/rocALUTION/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd rocALUTION-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DCMAKE_MODULE_PATH=/usr/hip/cmake \
  -DAMDGPU_TARGETS=$DEB_HIP_ARCHITECTURES \
  -DBUILD_CLIENTS_SAMPLES=OFF \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# hipcub
cd "$DEB_WORKSPACE"
wget -qO- https://github.com/ROCmSoftwarePlatform/hipCUB/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd hipCUB-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DCMAKE_MODULE_PATH=/usr/hip/cmake \
  -DAMDGPU_TARGETS=$DEB_HIP_ARCHITECTURES \
  -DBUILD_CLIENTS_SAMPLES=OFF \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# rocfft
cd "$DEB_WORKSPACE"
wget -qO- https://github.com/ROCmSoftwarePlatform/rocFFT/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd rocFFT-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DAMDGPU_TARGETS=$DEB_HIP_ARCHITECTURES \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# hipfft
cd "$DEB_WORKSPACE"
wget -qO- https://github.com/ROCmSoftwarePlatform/hipFFT/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd hipFFT-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DCMAKE_MODULE_PATH=/usr/hip/cmake \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install

# rocthrust
cd "$DEB_WORKSPACE"
wget -qO- https://github.com/ROCmSoftwarePlatform/rocThrust/archive/refs/tags/rocm-4.5.2.tar.gz | tar xz
cd rocThrust-rocm-4.5.2
cmake -S. -Bbuild -DLLD_DIR=/usr/lib/llvm-13/lib/cmake/lld -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=/usr/hip/bin/hipcc \
  -DAMDGPU_TARGETS=$DEB_HIP_ARCHITECTURES \
  -DCMAKE_INSTALL_PREFIX=/usr
make -j32 -C build
make -C build install
